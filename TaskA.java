package deva;

import java.util.HashMap;
import java.util.Map;

public class TaskA {

 
	    public static void main(String[] args) {
	        int[] foodCount = {10, 15, 20, 10, 11, 23, 20};
	        printDuplicatesAndSingles(foodCount);
	    }

	    public static void printDuplicatesAndSingles(int[] arr) {
	        Map<Integer, Integer> countMap = new HashMap<>();

	        // Count occurrences of each number in the array
	        for (int num : arr) {
	            countMap.put(num, countMap.getOrDefault(num, 0) + 1);
	        }

	        // Print duplicate numbers and numbers occurring only once
	        System.out.println("Repeated numbers in the array:");
	        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
	            if (entry.getValue() > 1) {
	                System.out.println(entry.getKey() + " (count: " + entry.getValue() + ")");
	            }
	        }

	        System.out.println("\nNumbers occurring only once in the array:");
	        for (Map.Entry<Integer, Integer> entry : countMap.entrySet()) {
	            if (entry.getValue() == 1) {
	                System.out.println(entry.getKey());
	            }
	        }
	    
	

        }

}
